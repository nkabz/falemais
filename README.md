# Telzir

Projeto feito com [Angular CLI](https://github.com/angular/angular-cli) versão 6.0.8.

## Development server

Executar `ng serve` na linha de comando para um dev server. A página será disponível em `http://localhost:4200/`.

## Sistema

Sistema desenvolvido por [Níkolas Abiuzzi](https://www.linkedin.com/in/nikolas-abiuzzi/) para o desafio da Vizir. 