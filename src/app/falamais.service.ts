import { Injectable } from '@angular/core';
import { buscaArray } from './buscaArray.model'
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FalamaisService {

  arrayChanged = new Subject<buscaArray[]>();
  
  dddsPossiveis: Array<Object> = [
    { ddd: "011", dddDestino: ["016", "017", "018"], dddValores: [1.9, 1.7, 0.9] },
    { ddd: "016", dddDestino: ["011"], dddValores: [2.9] },
    { ddd: "017", dddDestino: ["011"], dddValores: [2.7] },
    { ddd: "018", dddDestino: ["011"], dddValores: [1.9] }
  ]

  planosDisponiveis: Array<Object> = [
    { plano: "FaleMais 30", tempo: 30 },
    { plano: "FaleMais 60", tempo: 60 },
    { plano: "FaleMais 120", tempo: 120 }
  ];

  private buscasRealizadas: buscaArray[] = [];

  constructor() { }

  getPlanos() {
    return this.planosDisponiveis;
  }

  getDdds() {
    return this.dddsPossiveis;
  }

  getBuscaArray() {
    return this.buscasRealizadas;
  }

  insertBuscaArray(selectedOptionOrigem, selectedOptionDestino, selectedPlano, tempoMinutos) {

    /*
        Condicional pra ver se o DDD de origem possui uma rota para o DDD de destino
      */
    if (selectedOptionOrigem.dddDestino.includes(selectedOptionDestino.ddd)) {
      /*
        Aqui é feito o código para calcular o tempo com o FaleMais, a opção que utilizei foi usar dois arrays no objeto que contém os DDDs disponiveis
        e em cada posição de DDD destino no array, o valor da ligação correspondente em outro array, aí utilizo o index do array do DDD de destino para alcançar o valor no Array de valores
      */
      let resultado = (tempoMinutos - selectedPlano.tempo) * (selectedOptionOrigem.dddValores[selectedOptionOrigem.dddDestino.indexOf(selectedOptionDestino.ddd)]) * 1.1;
      /*
        Se o valor for negativo, ou seja, o tempo do plano cobre o número de minutos selecionado, o resultado inserido no objeto é 0
      */
      let resultadoComFaleMais = resultado > 0 ? resultado : 0;
      /*
        Cálculo sem FaleMais
      */
      let resultadoSemFaleMais = tempoMinutos * (selectedOptionOrigem.dddValores[selectedOptionOrigem.dddDestino.indexOf(selectedOptionDestino.ddd)]);

      /*
        Adicionamos em um Array de objetos as buscas realizadas
      */
      this.addItemBuscasArray(selectedOptionOrigem.ddd,
        selectedOptionDestino.ddd,
        tempoMinutos,
        selectedPlano.plano,
        (resultadoComFaleMais.toFixed(2)).toString(),
        (resultadoSemFaleMais.toFixed(2)).toString());
    }
    else {
      /*
        Se a busca realizada não obtiver um DDD de destino válido, a mesma é adicionada sem os resultados
      */
      this.addItemBuscasArray(selectedOptionOrigem.ddd,
        selectedOptionDestino.ddd,
        tempoMinutos,
        selectedPlano.plano,
        '-',
        '-');
    }


  }
  //Função para adicionar no Array
  addItemBuscasArray(origem, destino, tempo, plano, cFm, sFm) {

    let addedBusca = new buscaArray(origem, destino, tempo, plano, cFm, sFm);

    this.buscasRealizadas.unshift(addedBusca);
    this.arrayChanged.next(this.buscasRealizadas.slice());
  }

}
