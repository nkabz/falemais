import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FalamaisService } from '../../falamais.service'
import { buscaArray } from '../../buscaArray.model'
import { FalaMaisAnimations} from '../../animations'

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.css'],
  animations: [FalaMaisAnimations]
})
export class ResultadoComponent implements OnInit {

  buscasRealizadas: buscaArray[];

  subBuscas: Subscription;

  constructor(private faleMaisService: FalamaisService) { }

  ngOnInit() {
    this.subBuscas = this.faleMaisService.arrayChanged.subscribe((buscasRealizadas: buscaArray[]) => {
      this.buscasRealizadas = buscasRealizadas;
    })

    this.buscasRealizadas = this.faleMaisService.getBuscaArray();
  }

  ngOnDestroy() {
    this.subBuscas.unsubscribe();
  }

}
