import { Component, OnInit } from '@angular/core'
import { FalamaisService } from '../falamais.service'
import { FalaMaisAnimations } from '../animations';

@Component({
  selector: 'app-fala-mais-simulation',
  templateUrl: './fala-mais-simulation.component.html',
  styleUrls: ['./fala-mais-simulation.component.css'],
  animations: [FalaMaisAnimations]
})
export class FalaMaisSimulationComponent implements OnInit {

  selectedOptionOrigem: Object = null;
  selectedOptionDestino: Object = null;
  selectedPlano: Object = null;
  tempoMinutos: number = null;
  aparecerTabela = false;
  triedBusca = false;

  dddsPossiveis = this.faleMaisService.getDdds();

  planosDisponiveis = this.faleMaisService.getPlanos();

  constructor(private faleMaisService: FalamaisService) {  }

  ngOnInit() {

  }

  calculoFaleMais() {
    /*
      Esta condicional verifica se o botão de busca foi pressionado e se algum elemento do two-way binding ainda é nulo, que se for o caso,
      é mostrada uma mensagem de erro com a ajuda do helper triedBusca. Se o teste passar, a tabela aparece com a primeira busca.
    */
    if (!(this.selectedOptionDestino == null || this.selectedOptionOrigem == null || this.selectedPlano == null || this.tempoMinutos == null)) {

      this.aparecerTabela = true;
      this.triedBusca = false;

      this.faleMaisService.insertBuscaArray(this.selectedOptionOrigem, this.selectedOptionDestino, this.selectedPlano, this.tempoMinutos);

      this.clearSearch();

    } else {

      this.triedBusca = true;

      this.clearSearch();
    }
  }

  //Função para limpar a busca após cada utilização do Sistema
  clearSearch() {
    this.selectedOptionDestino = null;
    this.selectedOptionOrigem = null;
    this.selectedPlano = null;
    this.tempoMinutos = null;
  }

}
