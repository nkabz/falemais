import { Component } from '@angular/core';
import { FalaMaisAnimations } from './animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [FalaMaisAnimations]
})
export class AppComponent {

  animationState = 'in';

  appearFalaMais(){
    this.animationState = 'out';
  }
  
}
