import { trigger, state, style, transition,
    animate, group, query, stagger, keyframes
} from '@angular/animations';

export const FalaMaisAnimations = [
    trigger('slideInOut', [
        state('in', style({
            'max-height': '100%', 'opacity': '1', 'visibility': 'visible'
        })),
        state('out', style({
            'opacity': '0', 'visibility': 'hidden'
        })),
        transition('in => out', [group([
            animate('400ms ease-in-out', style({
                'opacity': '0'
            })),
            animate('700ms ease-in-out', style({
                'visibility': 'hidden'
            }))
        ]
        )]),
        transition('out => in', [group([
            animate('1ms ease-in-out', style({
                'visibility': 'visible'
            })),
            animate('600ms ease-in-out', style({
                'max-height': '100%'
            })),
            animate('800ms ease-in-out', style({
                'opacity': '1'
            }))
        ]
        )])
    ]),

    trigger('slideDownUp', [
        state('void', style({transform: 'translateY(100%)', opacity: 0})),
        state('*', style({transform: 'translateX(0)', opacity: 1})),
        transition(':leave, :enter', animate(400))
    ]),

    trigger('slideLeftRight', [
        state('void', style({transform: 'translateX(-100%)', opacity: 0})),
        state('*', style({transform: 'translateX(0)', opacity: 1})),
        transition(':leave, :enter', animate(500))
    ])
]