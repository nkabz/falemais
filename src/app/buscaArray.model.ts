export class buscaArray {
    public origem: string;
    public destino: string;
    public tempo: number;
    public plano: string;
    public cFm: string;
    public sFm: string;

    constructor(origem: string, destino: string, tempo: number, plano: string, cFm: string, sFm: string) {
        this.origem = origem;
        this.destino = destino;
        this.tempo = tempo;
        this.plano = plano;
        this.cFm = cFm;
        this.sFm = sFm;
    }
}

