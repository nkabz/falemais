import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FalaMaisSimulationComponent } from './fala-mais-simulation/fala-mais-simulation.component';
import { ResultadoComponent } from './fala-mais-simulation/resultado/resultado.component';

import { FalamaisService } from './falamais.service';
import { NumbersOnlyDirective } from './numbers-only.directive'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FalaMaisSimulationComponent,
    ResultadoComponent,
    NumbersOnlyDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [FalamaisService],
  bootstrap: [AppComponent]
})
export class AppModule { }
